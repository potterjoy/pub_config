# pub_config

#### 项目介绍
gradle 配置文件

为解决gradle 无关配置，将基础配置和公用jar版本配置到 public.gradle 和ext.gradle中

将个性化配置配置到 con.gradle中

让开发只关注代码开发和依赖jar包，而不用理会其他配置，简化代码中gradle配置


#### 安装教程

1. 配置con.gradle中仓库帐号密码和仓库地址，如果是企业，可以将仓库地址配置到ext中，统一管理
2. 将con.gradle 放到 GRADLE_HOME 根目录
3. ext.gradle 和 public.gradle 可以直接使用，也可以放到自己git中
4. ext.gradle 配置基础公共jar版本号
5. public.gradle 会给打好包的 MANIFESET.MF 中写入依赖JAR, git分支，git版本号，工程版本号等一系列参数便于运维管理

#### 使用说明

参考  DynamicDataSource 和 GenId 项目

#### 参与贡献

感谢 @李谨延 提供技术支持 https://gitee.com/jinyanroot

